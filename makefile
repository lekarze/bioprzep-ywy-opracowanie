all:	biop.tex
	pdflatex biop.tex
clean:
	-rm *.toc *.aux *.log *.out *.idx *.lof *.lot
build:
	pdflatex biop.tex
